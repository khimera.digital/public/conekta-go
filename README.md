[![pipeline status](https://gitlab.com/khimera.digital/public/conekta-go/badges/master/pipeline.svg)](https://gitlab.com/khimera.digital/public/conekta-go/commits/master) [![coverage report](https://gitlab.com/khimera.digital/public/conekta-go/badges/master/coverage.svg)](https://gitlab.com/khimera.digital/public/conekta-go/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/khimera.digital/public/conekta-go)](https://goreportcard.com/report/gitlab.com/khimera.digital/public/conekta-go)

### Go-Conekta
A Wrapper for use conekta's api v2 in golang inspired in [sait/go-conekta](https://github.com/sait/go-conekta)

This tutorial assumes the next:

* Have a conekta account
* Have a frontend that tokenize cards
* Knowledge of conekta's api

### Usage

First get the package

```
go get -u gitlab.com/khimera.digital/public/conekta-go/...
```

Import in your project

```go
import (
	"gitlab.com/khimera.digital/public/conekta-go/client"
	"gitlab.com/khimera.digital/public/conekta-go/customers"
	"gitlab.com/khimera.digital/public/conekta-go/models"
)
```

### Simple Example

```go
package main

import (
	"log"

	"gitlab.com/khimera.digital/public/conekta-go/client"
	"gitlab.com/khimera.digital/public/conekta-go/customers"
	"gitlab.com/khimera.digital/public/conekta-go/models"
)

func main() {
	client.APIKey = "your_private_key"
	customer, err := customers.Create(models.Customer{
		Name:  "Fulano Perez",
		Email: "fulano@example.com",
		Phone: "+5215555555555",
	})
	if err != nil {
		log.Println("Err: ", err)
		return
	}
	log.Println("Response: ", customer)

	// customers.Update(models.Customer{})
	// customers.Delete("customer_id")
	// customers.CreatePaymentSource("customer_id", models.PaymentSource{})
	// orders.Create(models.Order{})
	// ...
}
```
**You only need to set client.APIKey once**

### Resources

https://godoc.org/gitlab.com/khimera.digital/public/conekta-go

https://developers.conekta.com/libraries/javascript

https://developers.conekta.com/api

https://developers.conekta.com/tutorials/card

### Support

dev@khimera.io